# Matrix

## Setup

### Cloning existing projects
In order to complete the first project you need to have access
to the files of the project locally on your machine. Now, GitLab has some handy
features that allow us to maintain our own versions or projects as well as easily
obtain them.

First, you need to make sure that you have a GitLab account. If you do not you
can register [here](https://gitlab.com/users/sign_in).

Next, ensure that you have git on your machine. Try executing `git --help` from
the command line. If the git command is not found, you'll need to install git with
your package manager. For Debian: `sudo apt install git`.

Now, you need to fork and then clone this project, to do this:

1. Click 'Fork' on the project's main page ([here](https://gitlab.com/ct-starter-guide/project1)).
2. Click on your namespace and notice that the suffix of the url has changed from *'ct-starter-guide/project1'*
  to *'<your_namespace>/project1'* as you are now on your fork.
3. Next to the fork button, you should see a box with 'HTTPS' and a url. Click on the
right button 'Copy URL to clipboard' and then from your command line execute:
`git clone https://gitlab.com/<your_namespace>/project1.git`
4. Now change into the directory of the first project:
`cd project1`

You should now be in the directory of the first project. You'll see that this project
contains three files:

1. README.md
  - Most repositories have a README.md, this is just a simple file summarises a repository
  or directory within a repository.
2. matrix.py
  - You should write your code in this file.
3. matrix_test.py
  - This file contains tests which will test your code.


# The Problem

Given a string which represents a matrix of numbers, use the template matrix class in [matrix.py](matrix.py) to write two functions which return the rows and columns of that matrix.

For example, if you are given the string: "9 8 7\n5 3 2\n6 6 7", this would
produce the following matrix:

```text
9 8 7
5 3 2
6 6 7
```

Below illustrates how we can represent this matrix with indices:

```text
    0  1  2
  |---------
0 | 9  8  7
1 | 5  3  2
2 | 6  6  7
```

your code should be able to spit out:

- A list of the rows, reading each row left-to-right while moving
  top-to-bottom across the rows,
- A list of the columns, reading each column top-to-bottom while moving
  from left-to-right.

The rows for our example matrix:

- 9, 8, 7
- 5, 3, 2
- 6, 6, 7

And its columns:

- 9, 5, 6
- 8, 3, 6
- 7, 2, 7

Your functions must be able to return a list of a specified column or row.
For example, Matrix.row(1) should return `[5, 3, 2]`


## Running the tests
In order to test our [matrix class](matrix.py) works as expected, tests have been
written that use the [pytest framework](https://docs.pytest.org/en/latest/).

To run the tests, run the appropriate command below:

    pytest matrix_test.py

If this does not work, you probably don't have pytest installed as it's own package.
So, alternatively, you can try to Python to run the pytest module:

    python3 -m pytest matrix_test.py

If neither of these commands work, you'll have to install Pytest on your system.
If you're using debian, try:

    sudo apt install python3-pytest

To verify this has installed, try `pytest --help`, and you should see the Pytest
help message.

Consult your supervisor if you need help or once you have all the tests passing.


### Common `pytest` options

- `-v` : enable verbose output
- `-x` : stop running tests on first failure
- `--ff` : run failures from previous test before running other test cases

For other options, see `python -m pytest -h`
